package no.uib.inf101.datastructure;

public interface IGrid<T> extends GridDimension, GridCellCollection<T> {

  /**
   * Get the color of the cell at the given position.
   *
   * @param pos the position
   * @return the elemet of the cell
   * @throws IndexOutOfBoundsException if the position is out of bounds
   */
  T get(CellPosition pos);

  /**
   * Set the color of the cell at the given position.
   *
   * @param pos the position
   * @param T the new element
   * @throws IndexOutOfBoundsException if the position is out of bounds
   */
  void set(CellPosition pos, T elem);

}